
from urllib.request import urlopen, Request
from sys import argv
import os
from time import sleep

# RLE syntheses for linear-growth patterns (update these, but be careful
# that they're absolutely correct before committing because there are no
# validity checks):
always_include = '''
#CSYNTH yl78_1_20_ce2bb2c697ad330277935c32ef179035 costs 14 gliders (true).
x = 74, y = 80, rule = B3/S23
66bobo$66b2o$67bo14$39bo11bobo$39bobo9b2o$39b2o11bo3$54b3o$54bo$55bo7$
51bo$49b2o$50b2o4$23bo$24bo$22b3o46b3o$bo69bo$2bo69bo$3o46b3o$49bo$50b
o4$22b2o$23b2o$22bo7$18bo$19bo$17b3o3$21bo11b2o$21b2o9bobo$20bobo11bo
14$6bo$6b2o$5bobo!

#CSYNTH yl1344_2_1118_c2aaaf958f8f412c06554ab3e40b8749 costs 6 gliders (true).
x = 23, y = 23, rule = B3/S23
9bobo$10b2o$10bo6bo$16bo$16b3o$20b2o$20bobo$20bo2$o$b2o$2o5$3b2o$2bob
o$4bo2$5b3o$5bo$6bo!
'''

def partial_consistency_check(min_paths, apgcodes):
    '''
    Runs a consistency check against only the components involved in
    computing a specific set of objects.
    '''

    from shinjuku.transcode import decode_comp, realise_comp

    exhausted = set([])

    for cstr in apgcodes:
        curr = cstr
        while curr != "":
            pred = min_paths[curr]
            exhausted.add(pred[2])
            curr = pred[1]

    print('%d synthesis components required' % len(exhausted))

    elapsed = 0
    for line in exhausted:
        out_str = decode_comp(line)[2]
        pat = realise_comp(line)
        if out_str != pat.oscar(verbose=False, return_apgcode=True)["apgcode"]:
            raise ValueError("faulty component line " + line)
        elapsed += 1
        if elapsed % 100 == 0: print(elapsed, "components checked.")
    print("All", elapsed, "components checked successfully.")

def main():

    address = argv[2]

    knowns = urlopen(address + '/textcensus/b3s23/synthesis-costs').read().decode()
    knowns = [tuple(s.replace('"', '').split(',')) for s in knowns.split('\n') if ',' in s]
    knowns = {k : (int(v) % 100000000000000000) for (k, v) in knowns[1:]}

    # uncomment this to refresh all syntheses on Catagolue:
    # knowns = {'xp15_4r4z4r4': 3}

    from shinjuku.search import dijkstra, lookup_synth, lt

    min_paths = dijkstra(prefer_long=True)

    apgcodes = [k for (k, v) in min_paths.items() if ((len(k) > 0) and ((k not in knowns) or (knowns[k] > v[0])))]

    if ('xp15_4r4z4r4' in apgcodes):
        raise ValueError("No pentadecathlon found online!")

    apgcodes.append('xp15_4r4z4r4')
    apgcodes = [x for x in apgcodes if x.startswith('x')]

    print("%d total objects found" % len(min_paths))
    print("%d can be updated" % len(apgcodes))

    print("Preparing to update %s" % apgcodes)

    partial_consistency_check(min_paths, apgcodes)

    rles = '\n\n'.join([lt.pattern(x).rle_string() for x in apgcodes])
    with open("stdin.txt", 'w') as f:
        f.write(rles)

    print("Written RLEs.")

    os.system("cat stdin.txt | ./apgluxe -L 1 -t 1")

    logfiles = [x for x in os.listdir() if (len(x) > 5) and (x[:4] == 'log.')]

    trues = []

    for logfile in logfiles:

        print('Log file: %s' % logfile)

        with open(logfile) as f:
            trues += list(f.read().split())

    trues = set([l for l in trues if (len(l) > 0) and (l[0] == 'x') and (l in min_paths)])

    print('%d objects are true.' % len(trues))

    # Sorting the apgcodes reduces datastore costs by ensuring that each
    # batch involves as few tabulations as possible:
    apgcodes.sort()

    while (len(apgcodes) > 0):

        segment_size = min(1000, len(apgcodes))
        segment = apgcodes[:segment_size]
        apgcodes = apgcodes[segment_size:]

        payload = "%s SYNTH b3s23\n" % argv[1]
        rles = []

        if (len(apgcodes) == 0):

            rles.append(always_include)

        for apgcode in segment:

            if apgcode == 'xs0_0':
                N, pat = 0, lt.pattern('')
            else:
                N, pat = lookup_synth(min_paths, apgcode)

            rle = '\n#CSYNTH %s costs %d gliders (%s).\n' % (apgcode, N, ('true' if (apgcode in trues) else 'pseudo'))
            rle += pat.rle_string()
            rles.append(rle)

        payload += '\n'.join(rles)

        req = Request(address + "/commonnames", payload.encode('utf-8'), {"Content-type": "text/plain"})
        f = urlopen(req)

        print(f.read())
        print('%d apgcodes remaining' % len(apgcodes))

if __name__ == '__main__':

    main()
