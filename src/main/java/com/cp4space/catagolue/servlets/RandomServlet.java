
package com.cp4space.catagolue.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.lang.Iterable;
import java.util.TreeSet;
import java.util.Map.Entry;
import java.util.Map;
import java.util.TreeMap;
import java.util.HashMap;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cp4space.catagolue.census.Tabulation;
import com.cp4space.catagolue.census.Census;
import com.cp4space.catagolue.utils.SvgUtils;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.FetchOptions;

public class RandomServlet extends HttpServlet {

    public static String deterministicObject(String rulestring, String symmetry, double difficulty, String[] lines) {

        double lastdiff = 0.0;
        double newdiff = -1.0;
        String lastcode = null;

        TreeSet<String> avoidables = new TreeSet<>();

        for (String line : lines) {

            if (line.contains("difficulties") && (!line.contains("corderpuffer"))) {
                String[] eqparts = line.replace(" ", "").split("=");

                double thisdiff = Double.valueOf(eqparts[1].split(";")[0]);

                if (thisdiff <= difficulty) {
                    lastdiff = thisdiff;
                    lastcode = eqparts[0].split("\"")[1];
                    avoidables.add(lastcode);
                } else {
                    newdiff = thisdiff;
                    break;
                }
            }
        }

        if ((lastcode == null) || (lastcode.contains("_"))) { return lastcode; }

        lastdiff = 1.0 / lastdiff;
        newdiff = (newdiff > 0.0) ? (1.0 / newdiff) : 0.0;

        double lambda = (lastdiff - (1.0 / difficulty)) / (lastdiff - newdiff);

        Key tabulationKey = new KeyFactory.Builder("Census", rulestring + "/" + symmetry).addChild("Tabulation", lastcode).getKey();
        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

        try {

            Entity tabulation = datastore.get(tabulationKey);

            BufferedReader bufReader = Tabulation.getCensusStream(tabulation);

            TreeMap<Long, String> tmap = new TreeMap<>();

            String line = null;
            long cum = 0;

            try {
                while ((line = bufReader.readLine()) != null) {
                    String[] parts = line.split(" ");
                    String thiscode = parts[0];
                    if (avoidables.contains(thiscode)) { continue; }
                    tmap.put(cum, thiscode);
                    cum += Long.valueOf(parts[1]);
                }
            } catch(IOException e) {
                // This will never happen in practice
            }

            long pos = (long) (((double) cum) * lambda);
            Entry<Long, String> entry = tmap.floorEntry(pos);

            if (entry == null) { return null; }

            return entry.getValue();

        } catch (EntityNotFoundException e) {
            return null;
        }
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        resp.setContentType("text/plain");

        String hostname = req.getRemoteHost();
        if (hostname == null) { hostname = "null"; }

        String rulestring = req.getParameter("rule");
        String symmetry = req.getParameter("symmetry");
        String diffstring = req.getParameter("difficulty");
        String quantity = req.getParameter("quantity");
        String pathinfo = req.getPathInfo();

        if (pathinfo != null) {
            String[] pathParts = pathinfo.split("/");
            if ((pathParts.length >= 2) && (rulestring == null)) {
                rulestring = pathParts[1];
            }
            if ((pathParts.length >= 3) && (symmetry == null)) {
                symmetry = pathParts[2];
            }
            if ((pathParts.length >= 4) && (diffstring == null)) {
                diffstring = pathParts[3];
            }
            if ((pathParts.length >= 5) && (quantity == null)) {
                quantity = pathParts[4];
            }
        }

        PrintWriter writer = resp.getWriter();

        if ((rulestring == null) || rulestring.equals("")) { rulestring = "b3s23"; }
        if (  (symmetry == null) || symmetry.equals("")  ) { symmetry = "C1"; }
        if ((diffstring == null) || diffstring.equals("")) { diffstring = "1"; }
        if (  (quantity == null) || quantity.equals("")  ) { quantity = "1"; }

        int objects_to_return = Integer.valueOf(quantity);
        if (objects_to_return < 1 ) { objects_to_return = 1;  }
        if (objects_to_return > 30) { objects_to_return = 30; }

        String summary = LCDServlet.writeContent(null, rulestring, symmetry);
        String[] lines = summary.split("\n");

        double difficulty_target = Double.valueOf(diffstring) / objects_to_return;

        for (int i = 0; i < objects_to_return; i++) {

            double difficulty = difficulty_target / (0.0 - Math.log(Math.random()));
            writer.println("invfreq " + String.valueOf(difficulty));
            String apgcode = deterministicObject(rulestring, symmetry, difficulty, lines);
            if (apgcode != null) { writer.println("apgcode " + apgcode); }

        }
    }

}
