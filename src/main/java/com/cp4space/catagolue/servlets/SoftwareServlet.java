package com.cp4space.catagolue.servlets;

import java.io.BufferedReader;
import java.io.StringReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.net.URL;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.lang.Iterable;
import java.util.List;
import java.util.ArrayList;
import java.util.TreeSet;
import java.util.AbstractMap.SimpleEntry;
import java.util.LinkedHashMap;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.Map.Entry;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cp4space.catagolue.census.CommonNames;
import com.cp4space.catagolue.census.GzipUtil;
import com.cp4space.catagolue.census.Census;
import com.cp4space.catagolue.utils.SvgUtils;

import com.google.appengine.api.datastore.Blob;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;

import org.json.JSONObject;

public class SoftwareServlet extends ExampleServlet {

    @Override
    public void writeHead(PrintWriter writer, HttpServletRequest req) {
        writer.println("<script type=\"text/javascript\" src=\"/js/sorttable.js\"></script>");
    }

    @Override
    public String getTitle(HttpServletRequest req) {

        return "Software";

    }

    public void writeProgram(PrintWriter writer, String name, String url, String ... things) {

        writer.println("<tr><td><a href=\"" + url + "\">" + name + "</a></td>");
        for (String s : things) {
            writer.println("<td>" + s + "</td>");
        }
        writer.println("</tr>");

    }

    @Override
    public void writeContent(PrintWriter writer, HttpServletRequest req) {

        writer.println("<h3>Pattern editing and simulation</h3>");

        writer.println("<p>The most fully-featured and cross-platform editor/simulator is Golly 4.0, " +
            "available from <a href=\"https://sourceforge.net/projects/golly/files/golly/golly-4.0/\">here</a>. " +
            "It supports a large range of rule families including custom rules specified as rule tables. " +
            "The fast simulation algorithms were written by Tom Rokicki, and the intuitive user interface " +
            "was created by Andrew Trevorrow. Various other authors have also contributed to the source code " +
            "and extensive integrated pattern collection.</p>");

        writer.println("<p>Another highly functional pattern simulator is Chris Rowett's LifeViewer, " +
            "written in JavaScript and suitable for embedding into webpages. This is used on this site " +
            "in addition to the <a href=\"https://conwaylife.com/forums\">ConwayLife forums</a> and " +
            "<a href=\"https://conwaylife.com/wiki\">LifeWiki</a>.</p>");

        writer.println("<h3>Automated search programs</h3>");

        writer.println("<p>This is a more up-to-date version of <a href=\"https://www.ics.uci.edu/~eppstein/ca/search.html\">Life Search Programs</a>");
        writer.println("by David Eppstein, from where several of the program descriptions have been sourced. This departs from Eppstein's list in that");
        writer.println("only <b>extant open-source search programs</b> are included. Only the most up-to-date version of each program is included.</p>");

        writer.println("<table class=\"sortable\" cellspacing=1 border=2>");
        writer.println("<tr><th>Name</th><th>Author</th><th>Rulespace</th><th>Can find</th><th>Comments</th></tr>");

        // ---- Random search utilities ----

        writeProgram(writer, "apgsearch", "https://catagolue.hatsya.com/apgsearch", "Adam P. Goucher", "versatile", "natural objects",
            "Runs random seeds in an infinite universe and censuses the results. Supports a wide range of symmetries and custom rules. Results can be uploaded to Catagolue. Random asymmetric searches can be accelerated if you have a CUDA-compatible GPU.");

        writeProgram(writer, "gsearch", "https://www.ics.uci.edu/~eppstein/ca/gsearch.c", "David Eppstein", "outer-totalistic", "natural objects",
            "Performs a brute force search of all patterns fitting within a small rectangle (4x7 taking a day or two). Evolves each such pattern for a specified number of generations or until it repeats, grows too large, or matches a previously seen pattern. Recognizes spaceships, oscillators, unstable oscillators (such as Life queen bee and p90), replicators, and some puffers. Includes modes for finding symmetric patterns.");

        writeProgram(writer, "CopperSearch", "https://www.conwaylife.com/forums/viewtopic.php?f=9&t=2085&p=28953#p28922", "Alexey Nigen and Simon Ekstr&oslash;m", "Life only", "natural objects",
            "Similar to gsearch, but designed to allow the search space to easily be sharded into chunks suitable for distributed search.");

        writeProgram(writer, "RandomAgar", "http://www.gabrielnivasch.org/fun/life/", "Gabriel Nivasch", "outer-totalistic", "agars",
            "This program is intended to look for new Life oscillators, wicks, and agars. It generates random spatially periodic patterns, and runs them until they oscillate. It includes complete support for all possible symmetry types and clever oscillation detection algorithms.");

        // ---- Tree-based oscillator searchers ----

        writeProgram(writer, "ofind", "https://conwaylife.com/forums/viewtopic.php?f=9&t=3549", "David Eppstein and Dongook Lee", "outer-totalistic", "oscillators",
            "Similar breadth-first approach to gfind, but for finding oscillators instead of spaceships. It methodologically departs from gfind by explicitly handling stator cells differently from rotor cells, and extending all phases simultaneously.");

        writeProgram(writer, "osrc", "https://conwaylife.com/forums/viewtopic.php?p=90880#p90880", "Aidan F. Pierce", "isotropic", "oscillators and short wide spaceships",
            "A zfind-like oscillator search program, with special support for finding phoenices.");

        // ---- Tree-based spaceship searchers ----

        writeProgram(writer, "gfind", "https://conwaylife.com/wiki/Gfind", "David Eppstein", "outer-totalistic", "spaceships",
            "Very sophisticated breadth-first spaceship search program, described in <a href=\"https://arxiv.org/abs/cs/0004003\">his arXiv paper</a>. It extends partial patterns a row at a time, keeping track of rows in all phases of the pattern. Finds successor rows using a bit-parallel graph path computation; hash table eliminates redundant search branches; uses depth first iterated deepening, garbage collection, and row width reduction (triggered by excess depth in iterated deepening stages) to limit the breadth first queue size. Includes modes for finding symmetric patterns.");

        writeProgram(writer, "ntzfind", "https://github.com/rokicki/ntzfind", "Tom Rokicki and zdr", "isotropic", "spaceships",
            "Fast depth-first tree search, using a large amount of memory (8^width) as a lookup table. This program found the copperhead, the first c/10 spaceship.");

        writeProgram(writer, "qfind", "https://github.com/Matthias-Merzenich/qfind", "Matthias Merzenich", "isotropic", "spaceships",
            "A multithreaded tree search which incorporates ideas from both gfind and ntzfind.");

        writeProgram(writer, "ikpx2", "https://gitlab.com/apgoucher/ikpx2", "Adam P. Goucher", "isotropic", "spaceships",
            "A multithreaded tree search which incorporates many of the ideas from gfind, but uses SAT solvers for deep lookahead instead of depth-first search. This is a revamped version of the progrm which initially found Sir Robin. An integrated apgsearch is included to enable the finding of puffers and high-period spaceships, inspired by Paul Tooke's approach, and to periodically upload the results to Catagolue.");

        writeProgram(writer, "LSSS", "https://gitlab.com/andrew-j-wade/life_slice_ship_search", "Andrew J. Wade", "outer-totalistic", "spaceships",
            "A depth first spaceship search. Partial spaceships are represented by a graph of 2 cell wide slices through all generations of the spaceship. Requires plenty of hard disk space (many gigabytes are recommended).");

        writeProgram(writer, "knightt", "https://www.conwaylife.com/forums/viewtopic.php?f=9&t=1969#p28891", "Tim Coe", "Life only", "spaceships",
            "Exhaustive search program which determines the entire graph of spaceships of a particular speed, period, and width.");

        // ---- lifesrc variants ----

        writeProgram(writer, "LLS", "https://conwaylife.com/wiki/Logic_Life_Search", "Oscar Cunningham", "isotropic", "small patterns",
            "A highly versatile search program which converts cellular automata search problems into Boolean satisfiability problems and feeds them to a SAT solver.");

        writeProgram(writer, "JLS", "https://conwaylife.com/wiki/JavaLifeSearch", "Karel Suhajda", "outer-totalistic", "small patterns",
            "A Java reimplementation of David Bell's lifesrc. Excellent for finding low-period oscillators and sparkers.");

        writeProgram(writer, "rlifesrc", "https://alephalpha.github.io/rlifesrc/", "AlephAlpha", "isotropic and generations", "small patterns",
            "A Rust implementation of David Bell's lifesrc. Supports more rules than JLS or WLS.");

        // ---- signal circuitry search programs ('solid-state') ----

        writeProgram(writer, "dr2", "https://www.conwaylife.com/forums/viewtopic.php?f=9&t=1445", "Dean Hickerson and Matthias Merzenich", "outer-totalistic", "oscillators and signal circuitry",
            "Searches for patterns consisting of a small perturbation 'drifting' across a still-life background. Takes as input the perturbation and part of the background. Depth-first; simulates the evolution of the perturbation, making a choice whenever it needs the value of an unknown background cell, and backtracking whenever it finds an inconsistency in the background or the perturbation grows too large. Useful for finding high-period billiard-table oscillators, circuitry consisting of signals moving through static wires and components, and custom eaters.");

        writeProgram(writer, "Bellman", "https://github.com/simeksgol/BellmanWin_szlim", "Mike Playle, Michael Simkin, and Simon Ekstrom", "Life only", "catalysed reactions",
            "Attempts to catalyse reactions by including custom catalysts. This program was used by Mike Playle to discover the Snark, the smallest and fastest stable reflector in Life.");

        // ---- signal circuitry search programs (rearranging known catalysts) ----

        writeProgram(writer, "catgl", "https://www.conwaylife.com/forums/viewtopic.php?f=9&t=1378&p=11860", "Gabriel Nivasch, Dave Greene, and Michael Simkin", "Life only", "catalysed reactions",
            "Places still-life catalysts to react with a given pattern. The program uses an exhaustive depth-first traversal of all possibilities. It only places a catalyst at the moment that it will react with the pattern, using a data structure to make sure that the placement will not interfere with previous steps of the reaction. Dave Greene's modification adds support for escaping glider detection and removal.");

        writeProgram(writer, "ptbsearch", "https://www.ics.uci.edu/~eppstein/ca/ptbsearch.tar.gz", "Paul Callahan", "Life only", "catalysed reactions",
            "Attempts to perturb a starting reaction by adding combinations of eaters or other still lifes, to make a different reaction product without destroying the added patterns. Life evolution rule is hardcoded in source but easy to change. Useful for finding Herschel conduits and glider guns.");

        writeProgram(writer, "CatForce", "https://www.conwaylife.com/wiki/CatForce", "Michael Simkin", "Life only", "catalysed reactions",
            "Similar to catgl and ptbsearch, but allows for fully transparent catalysts provided they're restored sufficiently quickly.");

        // ---- collision-based search programs ----

        writeProgram(writer, "CollisionsSearch", "https://www.conwaylife.com/forums/viewtopic.php?f=9&t=2246", "Guam", "Life only", "catalysed reactions",
            "A Windows-only GUI search program with a similar algorithm to CatForce. This was used to discover the semi-Snark along with many new Herschel conduits.");

        writeProgram(writer, "gencols", "https://www.conwaylife.com/forums/viewtopic.php?f=9&t=495", "Paul Callahan", "Life only", "collision reactions",
            "Enumerates collisions between patterns (e.g. gliders and still lifes). Includes output filters to detect oscillators, spaceships, or successful eating of one pattern by another.");

        // ---- synthesis and destruction programs ----

        writeProgram(writer, "Collisearch", "https://conwaylife.com/forums/viewtopic.php?f=9&t=4064#p83075", "Alex Greason", "Life only", "glider syntheses",
            "Analyses random collisions between known constructible objects with one or more gliders in order to search for glider syntheses and synthesis components.");

        writeProgram(writer, "spark_search", "https://conwaylife.com/forums/viewtopic.php?f=9&t=4198", "Chris Cain", "Life only", "glider synthesis components",
            "Golly Python script for finding glider synthesis components with a desired effect.");

        writeProgram(writer, "GoL_destroy", "https://github.com/simeksgol/GoL_destroy", "Simon Ekstrom", "Life only", "destruction seeds",
            "Beam search for finding arrangements of still-lifes which coax an active reaction (typically an input glider) to result in the eventual clean self-destruction of the entire input. This uses hardware-vectorised bit-parallel algorithms for simulating Life on a bounded 256-by-256 grid.");

        // ---- high-level stable circuitry assembly programs ----

        writeProgram(writer, "Hersrch", "http://cranemtn.com/life/files/Hersrch/Hersrch-4Dec2012.zip", "Karel Suhajda", "Life only", "Herschel tracks",
            "Searches for open or closed Herschel tracks in Conway's Game of Life, using a database of known static and periodic track components. Backtracking search, pruning the search tree when the pattern exceeds given size bounds or when a self-overlapping sequence of components is detected (using an Aho-Corasick string matching automaton on a predefined list of impossible sequences).");

        writeProgram(writer, "slmake", "https://gitlab.com/apgoucher/slmake", "Adam P. Goucher", "Life only", "slow salvo syntheses",
            "Automates the construction of slow-salvo and single-channel constructions for arrangements of sufficiently separated simple still-lifes. This is most useful for programming universal constructors in order to create adjustable spaceships and replicators. Also includes HoneySearch, a parallelised reimplementation of Paul Chapman's Glue.");

        // --------

        writer.println("</table>");

    }
}
