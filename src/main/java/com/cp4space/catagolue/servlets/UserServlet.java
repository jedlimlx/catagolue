package com.cp4space.catagolue.servlets;

import java.io.PrintWriter;
import java.lang.Iterable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map.Entry;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.cp4space.catagolue.census.Census;
import com.cp4space.catagolue.census.CommonNames;
import com.cp4space.catagolue.census.Tabulation;
import com.cp4space.catagolue.utils.SvgUtils;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

public class UserServlet extends ExampleServlet {

    public static long userObjectCount(String censusName, String requestedName, DatastoreService datastore) {

        long objcount = 0;

        try {
            Key leaderboardKey = new KeyFactory.Builder("Census", censusName).addChild("Leaderboard", "numobjects").getKey();
            Entity leaderboardEntity = datastore.get(leaderboardKey);
            String[] lines = ((Text) leaderboardEntity.getProperty("data")).getValue().split("\n");
            for (String line : lines) {
                if (line != null && line.length() >= 1) {
                    String[] parts = line.split(" ", 2);
                    if (requestedName.equals(parts[1])) {
                        objcount = Long.valueOf(parts[0]);
                        break;
                    }
                }
            }
        } catch (EntityNotFoundException e) {

        }

        return objcount;
    }

    public static Set<Map.Entry<String, Long>> assimilateDiscoveries(String requestedName,
        Map<String, Long> disccount, PrintWriter writer) {

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

        List<Entity> attributions = new ArrayList<>();

        if (!requestedName.equals("#assimilated")) {
            Filter userFilter = new FilterPredicate("owner", FilterOperator.EQUAL, requestedName);
            Query query = new Query("Attribution").setFilter(userFilter);
            PreparedQuery preparedQuery = datastore.prepare(query);
            attributions = preparedQuery.asList(FetchOptions.Builder.withLimit(200));
        }

        Map<String, Long> ordlist = new HashMap<String, Long>();

        Key discKey = KeyFactory.createKey("Discoveries", requestedName);

        Entity discEntity;

        try {

            discEntity = datastore.get(discKey);
            String censusData = Tabulation.getArbitraryData(discEntity, "discoveryData");
            Census.affineAddToMap(censusData, (long) 1, ordlist, "");

        } catch (EntityNotFoundException e) {

            discEntity = new Entity(discKey);

        }

        for (Entity attrib : attributions) {

            String apgcode = (String) attrib.getProperty("apgcode");
            long ordinal = (long) attrib.getProperty("ordinal");

            if (ordlist.containsKey(apgcode)) {
                ordlist.put(apgcode, ordlist.get(apgcode) | (1 << (20 - ordinal)));
            } else {
                ordlist.put(apgcode, (long) (1 << (20 - ordinal)));
            }

            attrib.setProperty("owner", "#assimilated");
        }

        if (ordlist.containsKey("xp2_882030kgz010602")) { createBadge(datastore, requestedName, "Conchita"); }
        if (ordlist.containsKey("xp8_4b23021eaz57840c4d2")) { createBadge(datastore, requestedName, "Hitchhiker"); }
        if (ordlist.containsKey("xq7_3nw17862z6952")) { createBadge(datastore, requestedName, "Loafer"); }

        Tabulation tab = new Tabulation("");
        Set<Map.Entry<String, Long>> st = Census.sortTable(ordlist);

        for (Map.Entry<String, Long> entry : st) {
            String apgcode = entry.getKey();
            long ordinalbin = entry.getValue();
            tab.appendRow(apgcode, ordinalbin, 100000);
            String apgprefix = (apgcode.split("_"))[0];
            if (apgprefix.charAt(0) == 'x' && apgprefix.charAt(1) == 's') { apgprefix = "xs"; }
            if (disccount.containsKey(apgprefix)) {
                disccount.put(apgprefix, disccount.get(apgprefix) + 1);
            } else {
                disccount.put(apgprefix, (long) 1);
            }
        }

        tab.setArbitraryData(discEntity, "discoveryData");

        if (attributions.size() > 0) {
            datastore.put(discEntity);
            datastore.put(attributions);
        }

        boolean avail_limitless = true;
        boolean avail_voyager = true;
        boolean avail_monarchist = true;
        boolean avail_gemini = true;

        for (Map.Entry<String, Long> entry : st) {

            String apgcode = entry.getKey();
            long ordinalbin = entry.getValue();

            if (apgcode.substring(0, 2).equals("yl")) {
                if (avail_limitless) {createBadge(datastore, requestedName, "Limitless"); avail_limitless = false; }
            } else if (apgcode.substring(0, 2).equals("xq")) {
                if (avail_voyager) {createBadge(datastore, requestedName, "Voyager"); avail_voyager = false; }
            } else if (apgcode.substring(0, 5).equals("xp30_") && (ordinalbin >= 524288)) {
                if (avail_monarchist) {createBadge(datastore, requestedName, "Monarchist"); avail_monarchist = false; }
            } else if (apgcode.substring(0, 5).equals("xp46_") && (ordinalbin >= 524288)) {
                if (avail_gemini) {createBadge(datastore, requestedName, "Gemini"); avail_gemini = false; }
            }
        }

        return st;
    }

    public static void createBadge(DatastoreService datastore, String username, String badgename) {

        Entity entity = new Entity("Badge", badgename, KeyFactory.createKey("Discoveries", username));
        datastore.put(entity);

    }

    public void writeBadges(PrintWriter writer, String requestedName) {

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

        Key discKey = KeyFactory.createKey("Discoveries", requestedName);

        Query query = new Query("Badge", discKey);
        PreparedQuery preparedQuery = datastore.prepare(query);
        List<Entity> badges = preparedQuery.asList(FetchOptions.Builder.withLimit(1000));

        Map<String, String> namemap = new HashMap<String, String>();
        CommonNames.getNamemap("badges", namemap, false);

        if (badges.size() > 0) {

            writer.println("<h3><a name=\"badges\">Badges</a></h3>");
            writer.println("<table>");

            for (Entity badge : badges) {
                String badgename = badge.getKey().getName();
                String description = "A badge with no description";
                if (namemap.containsKey(badgename)) {
                    description = namemap.get(badgename);
                }

                writer.println("<tr><td><img src=\"/images/badges/" + badgename + ".png\" /></td>");
                writer.println("<td><b>" + badgename + ":</b> " + description + "</td></tr>");

            }

            writer.println("</table>");

        }

    }

    public void writeChart(PrintWriter writer, String requestedName, boolean monthly) {

        writer.println("<h3><a name=\"barchart\">Contribution by date</a></h3>");

        int columns = 31;

        long[] totalCount = new long[columns];
        long[] userCount = new long[columns];

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

        List<Entity> censuses = new ArrayList<Entity>();

        if (monthly) {
            writer.println("<p>Switch to <a href=\"/user/" + requestedName.replaceAll(" ", "%20") + "/daily#barchart\">daily</a></p>");
            Query query = new Query("Census").addSort("numobjects", Query.SortDirection.ASCENDING);
            PreparedQuery preparedQuery = datastore.prepare(query);
            List<Entity> oldcensuses = preparedQuery.asList(FetchOptions.Builder.withLimit(1));

            String firstsym = "b3s23/C1-2018-12-01";

            for (Entity censusEntity : oldcensuses) {
                String keyname = censusEntity.getKey().getName();
                firstsym = keyname.substring(0, keyname.length() - 2) + "01";
            }

            for (int i = 0; i < columns; i++) {
                try {
                    Key censusKey = KeyFactory.createKey("Census", firstsym);
                    censuses.add(datastore.get(censusKey));
                } catch (EntityNotFoundException e) {
                    // This will never happen.
                }
                int month = Integer.valueOf("1" + firstsym.split("-")[2]) - 101;
                int year = Integer.valueOf(firstsym.split("-")[1]);
                if (month == 0) { month = 12; year -= 1; }
                firstsym = "b3s23/C1-" + year + "-" + String.valueOf(100 + month).substring(1) + "-01";
            }
        } else {
            writer.println("<p>Switch to <a href=\"/user/" + requestedName.replaceAll(" ", "%20") + "/monthly#barchart\">monthly</a></p>");
            Query query = new Query("Census").addSort("numobjects", Query.SortDirection.ASCENDING);
            PreparedQuery preparedQuery = datastore.prepare(query);
            censuses = preparedQuery.asList(FetchOptions.Builder.withLimit(columns));
        }        

        // writer.println("<ul>");

        int datenum = 0;

        long prevuser = userObjectCount("b3s23/C1", requestedName, datastore);
        long prevtotal = 0;

        if (prevuser >= 1000000000000L) {createBadge(datastore, requestedName, "Trillionaire"); }
        if (prevuser >= 10000000000000L) {createBadge(datastore, requestedName, "Gigamyriad"); }

        try {
            Key censusKey = KeyFactory.createKey("Census", "b3s23/C1");
            Entity censusEntity = datastore.get(censusKey);
            prevtotal = (long) censusEntity.getProperty("numobjects");
        } catch (EntityNotFoundException e) {
            // This will never happen.
        }

        String dateData = (monthly ? "'this month'" : "'today'");

        for (Entity censusEntity : censuses) {
            String keyname = censusEntity.getKey().getName();
            String thedate = "'" + keyname.split("-", 2)[1] + "'";
            long numobjects = 0 - ((long) censusEntity.getProperty("numobjects"));
            long userobjects = userObjectCount(keyname, requestedName, datastore);

            totalCount[datenum] = prevtotal - numobjects;
            prevtotal = numobjects;
            userCount[datenum] = prevuser - userobjects;
            prevuser = userobjects;

            dateData = thedate + ", " + dateData;

            // writer.println("<li>" + totalCount[datenum] + " -- " + userCount[datenum] + "</li>");

            datenum += 1;
        }

        dateData = dateData.split(",", 2)[1];

        String userData = "";
        String restData = "";

        for (int i = datenum - 1; i >= 0; i--) {

            userData += userCount[i];
            restData += (totalCount[i] - userCount[i]);

            if (i > 0) {
                restData += ", ";
                userData += ", ";
            }
        }

        // writer.println("</ul>");

        writer.println("<script src=\"https://code.highcharts.com/highcharts.js\"></script>");
        writer.println("<script src=\"https://code.highcharts.com/highcharts-3d.js\"></script>");
        writer.println("<script src=\"https://code.highcharts.com/modules/exporting.js\"></script>");

        writer.println("<script type=\"text/javascript\">");
        writer.println("$(function () {");
        writer.println("    $('#contribprop').highcharts({");
        writer.println("        chart: {backgroundColor: '#C0FFEE', type: 'column', options3d: {enabled: true, alpha: 20, beta: 0, viewDistance: 25, depth: 40}},");
        writer.println("        title: {text: 'Objects contributed by " + requestedName + " versus total contribution'},");
        writer.println("        tooltip: {pointFormat: '<span style=\"color:{point.color}\">{series.name}:</span> <b>{point.y}</b><br/>', shared: true},");
        writer.println("        plotOptions: {column: {stacking: 'normal', pointPadding: 0.05, groupPadding: 0}},");
        writer.println("        xAxis: {categories: [" + dateData + "]},");
        writer.println("        yAxis: {min: 0},");
        writer.println("        series: [{name: 'Everyone else', data: [" + restData + "]}, {name: '" + requestedName + "', data: [" + userData + "]}]");
        writer.println("    });");
        writer.println("});");

        writer.println("</script>");

        writer.println("<div id=\"contribprop\" style=\"height: 480px\"></div>");

    }

    @Override
    public String getTitle(HttpServletRequest req) {

        return "User";
    }

    @Override
    public void writeHead(PrintWriter writer, HttpServletRequest req) {

        writer.println("<script type=\"text/javascript\" src=\"/js/jquery.min.js\"></script>");

    }

    @Override
    public void writeContent(PrintWriter writer, HttpServletRequest req) {

        String requestedName = req.getParameter("user");
        String prefixName = req.getParameter("discoveries");
        String pathinfo = req.getPathInfo();

        if (pathinfo != null) {
            String[] pathParts = pathinfo.split("/");
            if ((pathParts.length >= 2) && (requestedName == null)) {
                requestedName = pathParts[1];
            }
            if ((pathParts.length >= 3) && (prefixName == null)) {
                prefixName = pathParts[2];
            }
        }

        if (requestedName == null) {

            writer.println("<p>No username provided.</p>");

        } else {

            writeBadges(writer, requestedName);

            writer.println("<h3>Recent hauls</h3>");

            writer.println("<p>The most recent hauls submitted by " + requestedName + " are available <a href=\"/haul/b3s23/C1?user=" + requestedName.replaceAll(" ", "%20") + "\">here</a>.</p>");

            writeChart(writer, requestedName, (prefixName != null) && prefixName.equals("monthly"));

            writer.println("<h3><a name=\"discoveries\">Discoveries</a></h3>");

            // Update the datastore to assimilate recent discoveries

            Map<String, Long> disccount = new HashMap<String, Long>();
            Set<Map.Entry<String, Long>> st = assimilateDiscoveries(requestedName, disccount, writer);

            if ((prefixName == null) || prefixName.equals("monthly") || prefixName.equals("daily")) {

                Set<Map.Entry<String, Long>> st2 = Census.sortTable(disccount);

                writer.println("<ul>");

                long runningtotal = 0;

                for (Map.Entry<String, Long> entry : st2) {
                    String apgprefix = entry.getKey();
                    long thecount = entry.getValue();
                    runningtotal += thecount;
                    writer.println("<li><a href=\"/user/" + requestedName.replaceAll(" ", "%20") + "/" + apgprefix + "#discoveries\">" + (apgprefix.equals("xs") ? "<b>Still lifes</b>" : apgprefix) + "</a> (" + String.valueOf(thecount) + " objects)</li>");
                }

                writer.println("<li><a href=\"/user/" + requestedName.replaceAll(" ", "%20") + "/all#discoveries\"><b>All discoveries</b></a> (" + String.valueOf(runningtotal) + " objects)</li>");

                writer.println("</ul>");

            } else {

                String ordnames[] = {"<b>first</b>", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth", "eleventh", "twelfth", "thirteenth", "fourteenth", "fifteenth", "sixteenth", "seventeenth", "eighteenth", "nineteenth", "twentieth"};

                writer.println("<p>" + requestedName + " found at least one of the first 20 of each of the following objects:</p>");

                writer.println("<table cellspacing=1 border=2 cols=2>");
                writer.println("<tr><th>Image</th><th>Object</th><th>Ordinal</th></tr>");

                for (Map.Entry<String, Long> entry : st) {

                    String apgcode = entry.getKey();
                    long ordinalbin = entry.getValue();
                    String ordnamelist = null;

                    for (int i = 1; i <= 20; i++) {
                        if ((ordinalbin & (1 << (20 - i))) > 0) {
                            ordnamelist = (ordnamelist == null) ? ordnames[i - 1] : (ordnamelist + ", " + ordnames[i - 1]);
                        }
                    }

                    String apgprefix = (apgcode.split("_"))[0];
                    if (prefixName.equals("all") || prefixName.equals(apgprefix) || prefixName.equals(apgprefix.substring(0, 2))) {
                        SvgUtils.apgRow(writer, "b3s23", apgcode, ordnamelist);
                    }
                }

                writer.println("</table>");
            }
        }
    }
}
