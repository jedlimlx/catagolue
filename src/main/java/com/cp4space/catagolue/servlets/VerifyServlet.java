package com.cp4space.catagolue.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cp4space.catagolue.census.Census;
import com.cp4space.catagolue.census.Tabulation;
import com.cp4space.catagolue.parmenides.Parmenides;
import com.cp4space.payosha256.PayoshaUtils;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Text;
import com.google.apphosting.api.ApiProxy;

public class VerifyServlet extends HttpServlet {

    private static final Logger log = Logger.getLogger(Census.class.getName());

    public static boolean isStandard(String apgcode) {

        if (apgcode.equals("PATHOLOGICAL")) { return true; }
        if (apgcode.length() < 3) { return false; }

        String x = apgcode.substring(0, 2);

        if (x.equals("xp")) { return true; }
        if (x.equals("xq")) { return true; }
        if (x.equals("xs")) { return true; }
        if (x.equals("yl")) { return true; }
        if (x.equals("zz")) { return true; }
        if (x.equals("ov")) { return true; }
        if (x.equals("me")) { return true; }

        return false;

    }

    public void submitVerification(BufferedReader reader, PrintWriter writer, String displayedName) throws IOException {

        String rulestring = "b3s23";
        String symmetry = "C1";
        String md5 = "none";
        String passcode = "unknown";

        StringBuilder claimedData = new StringBuilder();

        String line = null;

        while ((line = reader.readLine()) != null) {
            if (line.length() >= 2) {
                if (line.charAt(0) == '@') {
                    // Header line:
                    String[] parts = line.substring(1).split(" ", 2);

                    try {
                        if (parts[0].equalsIgnoreCase("MD5") && parts.length >= 2) {
                            md5 = parts[1];
                        } else if (parts[0].equalsIgnoreCase("PASSCODE") && parts.length >= 2) {
                            passcode = parts[1];
                        } else if (parts[0].equalsIgnoreCase("RULE") && parts.length >= 2) {
                            rulestring = parts[1].toLowerCase();
                        } else if (parts[0].equalsIgnoreCase("SYMMETRY") && parts.length >= 2) {
                            symmetry = parts[1].replace("G", "C").replace("H", "D");
                        }
                    } catch (NumberFormatException e) {
                        // To stop saboteurs from crashing Catagolue by posting invalid data.
                    }
                } else {
                    // Ordinary line:
                    claimedData.append(line + "\n");
                }
            }
        }

        if (passcode.equals(Parmenides.remd5(md5))) {

            log.warning("Passcode is correct.");

            DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

            Entity haul = null;
            Double chisquare = 0.0;

            try {
                Key haulKey = new KeyFactory.Builder("Census", rulestring + "/" + symmetry).addChild("Haul", md5).getKey();
                haul = datastore.get(haulKey);
                chisquare = (Double) haul.getProperty("chisquare");
            } catch (EntityNotFoundException e) {
                symmetry = symmetry.replace("C", "G").replace("D", "H");
                try {
                    Key haulKey = new KeyFactory.Builder("Census", rulestring + "/" + symmetry).addChild("Haul", md5).getKey();
                    haul = datastore.get(haulKey);
                    Double gpu_chisquare = (Double) haul.getProperty("chisquare");
                    if ((gpu_chisquare != null) && (gpu_chisquare >= 1.0e7)) { chisquare = gpu_chisquare; }
                } catch (EntityNotFoundException f) {
                    return;
                }
            }

            String privateData = Tabulation.getArbitraryData(haul, "privateData");
            Long status = (Long) haul.getProperty("committed");

            if ((status == null || status == Census.REJECTED || status == Census.UNVERIFIED)) { 

                String errorMessage = null;

                String[] lines = privateData.split("\n");

                for (String pline : lines) {

                    if (pline.length() >= 3) {

                        String[] parts = pline.split(" ");

                        if (parts[2].equals("false")) {
                            errorMessage = "Invalid apgcode: "+parts[0];
                        }
                    }
                }

                double chisquare_threshold = 1.0;

                if ((chisquare != null) && (chisquare > 1.0)) {
                    Entity cachedCensus = Parmenides.retrieveTable(rulestring, symmetry);
                    Double tcs = (Double) cachedCensus.getProperty("meanchisquare");
                    chisquare_threshold = (tcs + 10.0) * 1.7;
                }

                if (errorMessage != null) {

                } else if (chisquare == null) {

                    errorMessage = "No chi-square test statistic exists.";

                } else if (chisquare <= chisquare_threshold) {

                    Map<String, Long> firstMap = new HashMap<String, Long>();
                    Map<String, Long> secondMap = new HashMap<String, Long>();

                    log.warning("Comparing private data with claimed data: ");

                    Census.affineAddToMap(privateData, 1, firstMap, "");
                    Census.affineAddToMap(claimedData.toString(), 1, secondMap, "");

                    for (String apgcode : firstMap.keySet()) {
                        if (firstMap.get(apgcode) == null) {
                            errorMessage = (apgcode + " not found in first map.");
                        } else if (firstMap.get(apgcode) < 0) {
                            errorMessage = (apgcode + " occurred a negative number of times.");
                        } else if (secondMap.get(apgcode) == null) {
                            errorMessage = (apgcode + " not found in second map.");
                        } else if (secondMap.get(apgcode) < Math.min(10, firstMap.get(apgcode)) - 1) {
                            errorMessage = ("Insufficiently many occurrences of " + apgcode + " in second map.");
                        } else if (!isStandard(apgcode)) {
                            errorMessage = ("Hauls containing non-standard objects are automatically rejected.");
                        } else {
                            // log.warning("Correct number of occurrences (namely "+String.valueOf(secondMap.get(apgcode))+") of " + apgcode);
                        }
                    }

                    log.warning("Comparison is complete.");
                } else {
                    errorMessage = "Haul fails chi-square test: "+String.valueOf(chisquare)+" > "+String.valueOf(chisquare_threshold);
                }

                boolean reflexive = false;

                if (errorMessage == null) {
                    log.warning("Haul "+md5+" passed verification.");
                    haul.setProperty("committed", Census.VERIFIED);
                    String submitterName = (String) haul.getProperty("displayedName");
                    haul.setProperty("displayedName", submitterName.replace("#", "+"));
                    haul.setProperty("errorMessage", "success");
                } else {
                    log.warning("Haul "+md5+" failed verification.");
                    log.severe(errorMessage);
                    haul.setProperty("committed", Census.REJECTED);
                    haul.setProperty("errorMessage", errorMessage);
                }

                haul.setUnindexedProperty("verifierName", displayedName.replace("#", "+"));
                datastore.put(haul);
            }

        } else {
            log.warning("Passcode is incorrect.");
        }

    }


    public boolean getHaulCertificate(BufferedReader reader, PrintWriter writer, String verifierName) throws IOException {

        String rulestring = reader.readLine();
        String symmetry = reader.readLine();

        String cpu_symmetry = symmetry.replace("G", "C").replace("H", "D");
        String gpu_symmetry = symmetry.replace("C", "G").replace("D", "H");

        if (ghcSymmetry(rulestring, cpu_symmetry, writer, verifierName, symmetry)) {
            return true;
        } else if (ghcSymmetry(rulestring, gpu_symmetry, writer, verifierName, symmetry)) {
            return true;
        } else {
            writer.println("no");
            return false;
        }

    }

    public boolean ghcSymmetry(String rulestring, String symmetry, PrintWriter writer, String verifierName, String oldsym) throws IOException {

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Key censusKey = KeyFactory.createKey("Census", rulestring + "/" + symmetry);

        log.warning("Rulestring: "+rulestring+", symmetry: "+symmetry);

        Filter propertyFilter = new FilterPredicate("committed",
                FilterOperator.EQUAL,
                Census.UNVERIFIED);

        Query query = new Query("Haul", censusKey).setFilter(propertyFilter).addSort("date", Query.SortDirection.DESCENDING);
        query = query.setKeysOnly();
        PreparedQuery preparedQuery = datastore.prepare(query);

        int limit = 100;

        List<Entity> hauls = preparedQuery.asList(FetchOptions.Builder.withLimit(limit));

        if (hauls.size() > 0) {
            // We randomly decide which haul to process:

            Entity haul = null;
            boolean reflexive = true;

            for (int i = 0; i < 5; i++) {
                // Try multiple times to find a haul by a different contributor:

                int randomNumber = Long.valueOf(ApiProxy.getCurrentEnvironment().getRemainingMillis()).intValue();
                randomNumber = randomNumber % hauls.size();
                Entity newhaul = hauls.get(randomNumber % hauls.size());
                try {
                    haul = datastore.get(newhaul.getKey());
                } catch (EntityNotFoundException e) {
                    continue;
                }

                String submitterName = (String) haul.getProperty("displayedName");

                if (verifierName.equals("Continuous Integration") && submitterName.equals("Anonymous")) { continue; }

                reflexive = submitterName.equals(verifierName) || verifierName.equals("Anonymous");

                if (!submitterName.equals(verifierName)) { break; }
            }

            if (haul == null) { return false; }
            if (reflexive && (hauls.size() * 2 < limit)) { return false; }

            String publicData = Tabulation.getArbitraryData(haul, "publicData");

            publicData = publicData.replace(symmetry + "/", oldsym + "/");

            String md5 = haul.getKey().getName();
            String md5b = Parmenides.remd5(md5);

            writer.println("yes");
            writer.println(md5);
            writer.println(md5b);
            writer.println(publicData);
            return true;
        } else {
            return false;
        }

    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        resp.setContentType("text/plain");

        BufferedReader reader = req.getReader();
        PrintWriter writer = resp.getWriter();

        String payoshaRequest = reader.readLine();

        String payoshaResponse = PayoshaUtils.callPayosha("catagolue", payoshaRequest);
        String[] parts = payoshaResponse.split(":", 4);

        if ((parts[0].equals("payosha256")) && (parts[1].equals("good"))) {
            writer.println("Payosha256 authentication succeeded.");

            if (parts[2].equals("verify_apgsearch_haul")) {
                getHaulCertificate(reader, writer, parts[3]);
            } else if (parts[2].equals("submit_verification")) {
                submitVerification(reader, writer, parts[3]);
            }
        } else {
            writer.println("Payosha256 authentication failed with the following response:");
            writer.println(payoshaResponse);
        }

        writer.println("***********************************************");
    }

}
