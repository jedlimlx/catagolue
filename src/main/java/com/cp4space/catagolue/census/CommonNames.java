package com.cp4space.catagolue.census;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;

public class CommonNames {
    
    public static void putNamemap(String namespace, Map<String, String> namemap) {
        
        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        
        Entity commonnameEntity = new Entity("CommonNames", namespace);
        
        String commonnameData = null;
        
        for (Entry<String, String> entry : namemap.entrySet()) {
            if (commonnameData == null) {
                commonnameData = entry.getKey() + " " + entry.getValue();
            } else {
                commonnameData += ("\n" + entry.getKey() + " " + entry.getValue());
            }
        }
        
        if (commonnameData == null) {
            commonnameData = "";
        }
        
        commonnameEntity.setProperty("data", new Text(commonnameData));
        datastore.put(commonnameEntity);
    }
    
    public static void getNamemap(String namespace, Map<String, String> namemap, boolean mogrify) {
        
        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        
        Key commonnameKey = new KeyFactory.Builder("CommonNames", namespace).getKey();

        try {
            Entity commonnameEntity = datastore.get(commonnameKey);

            String commonnameData = ((Text) commonnameEntity.getProperty("data")).getValue();
            
            String[] lines = commonnameData.split("\n");
            
            for (String line : lines) {
                
                if (line.length() >= 2) {
                
                    String[] parts = line.split(" ", 2);

                    String description = parts[1];
                    if (mogrify) {
                        String[] parts2 = description.split("!");
                        if (parts2.length == 2) {
                            description = "<a href=\"http://conwaylife.com/wiki/" + parts2[1] + "\">" + parts2[0] + "</a>";
                        }
                    }

                    namemap.put(parts[0], description);
                }
            }
        } catch (EntityNotFoundException e) {
            
        }
        
    }

}
