package com.cp4space.payosha256;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.lang.Math;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

public class PayoshaUtils {

    final protected static char[] hexArray = "0123456789abcdef".toCharArray();

    /**
     * Converts a byte array into a hexadecimal String.
     * 
     * @param bytes  a byte array.
     * @return  the lower-case hexadecimal representation of the array.
     */
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 255;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 15];
        }
        return new String(hexChars);
    }

    public static boolean notInProduction() {
        /*
        * Determine whether we are running in production:
        */

        long numobjects = 0;

        try {
            DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
            Key censusKey = KeyFactory.createKey("Census", "b3s23/C1");
            Entity censusEntity = datastore.get(censusKey);
            numobjects = (long) censusEntity.getProperty("numobjects");
        } catch (EntityNotFoundException e) {

        }

        long exp24 = 16777216;
        long exp48 = exp24 * exp24;

        return (numobjects < exp48);
    }

    public static boolean validatePassword(String password) {

        if (notInProduction() && password.equals("test_password")) { return true; }

        String hashedPassword = "";

        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(password.getBytes("UTF-8"));
            hashedPassword = PayoshaUtils.bytesToHex(md.digest());
        } catch (UnsupportedEncodingException e) {

        } catch (NoSuchAlgorithmException e) {

        }

        return hashedPassword.equals("45849b449e61b099bf5965118ac8d0150c0cad0ff21f6541e01f5cb0e76349d9");

    }

    /**
     * Returns a list of extant payosha256 keys associated with the current user.
     * 
     * @param payoshaName  the name of the payosha256 instance to query.
     * @return  a list of DataStore entities representing the keys.
     */
    public static List<Entity> getPayoshaKeys(String payoshaName) {

        UserService userService = UserServiceFactory.getUserService();
        User user = userService.getCurrentUser();

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Key threadKey = KeyFactory.createKey("Payosha", payoshaName);
        Filter propertyFilter = new FilterPredicate("user",
                FilterOperator.EQUAL,
                user);
        Query query = new Query("PayoshaKey", threadKey).setFilter(propertyFilter).addSort("date", Query.SortDirection.DESCENDING);
        PreparedQuery preparedQuery = datastore.prepare(query);
        List<Entity> payoshaKeys = preparedQuery.asList(FetchOptions.Builder.withLimit(20));

        return payoshaKeys;
    }

    /**
     * Creates a new payosha256 key associated with the current user.
     * 
     * @param payoshaName  the name of the payosha256 instance to query.
     * @param payoshaKey  the alphanumeric string to use as the payosha256 key.
     * @param displayedName  the string to use as the displayed username associated with the key.
     */
    public static boolean postKey(String payoshaName, String payoshaKey, String displayedName) {

        Key threadKey = KeyFactory.createKey("Payosha", payoshaName);

        Date date = new Date();
        UserService userService = UserServiceFactory.getUserService();
        User user = userService.getCurrentUser();

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

        try {

            Key keyKey = new KeyFactory.Builder("Payosha", payoshaName).addChild("PayoshaKey", payoshaKey).getKey();
            Entity existingKey = datastore.get(keyKey);
            return true;

        } catch (EntityNotFoundException e) {

            Entity comment = new Entity("PayoshaKey", payoshaKey, threadKey);
            comment.setProperty("user", user);
            comment.setProperty("date", date);
            comment.setProperty("payoshaKey", payoshaKey);
            comment.setProperty("displayedName", displayedName);

            datastore.put(comment);
            return false;

        }
    }

    /**
     * Checks that the provided proof-of-work meets the target.
     * @param payoshaName  the name of the payosha256 instance to query.
     * @param tokenString  the string representing the payosha256 token.
     * @param nonce  a number to append to the token (with an intermediate colon) to give the proof-of-work.
     * @return  the response from the payosha256 instance.
     */
    public static String payToken(String payoshaName, String tokenString, String nonce) {

        MessageDigest md;
        try {

            md = MessageDigest.getInstance("SHA-256");
            String prehash = tokenString + ":" + nonce;
            md.update(prehash.getBytes("UTF-8"));
            String posthash = bytesToHex(md.digest());

            DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
            Key tokenKey = new KeyFactory.Builder("PayoshaToken", tokenString).getKey();
            Entity token = datastore.get(tokenKey);
            String target = (String) token.getProperty("target");

            if (posthash.compareTo(target) <= 0) {
                String displayedName = (String) token.getProperty("displayedName");
                String operationName = (String) token.getProperty("operation");
                datastore.delete(tokenKey);
                return "payosha256:good:" + operationName + ":" + displayedName;
            } else {
                return "payosha256:error:Hash exceeds target " + target;
            }

        } catch (EntityNotFoundException e) {
            return "payosha256:error:Server does not recognise token.";
        } catch (NoSuchAlgorithmException e) {
            return "payosha256:error:Server does not recognise SHA-256 hashing algorithm.";
        } catch (UnsupportedEncodingException e) {
            return "payosha256:error:Server does not recognise UTF-8 encoding.";
        }

    }

    public static void createOperation(String payoshaName, String operationName, String target) {

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Key threadKey = KeyFactory.createKey("Payosha", payoshaName);

        Entity operation = new Entity("PayoshaOperation", operationName, threadKey);
        operation.setProperty("target", target);
        datastore.put(operation);

    }

    public static String getToken(String payoshaName, String keyName, String operationName) {

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

        Key operationKey = new KeyFactory.Builder("Payosha", payoshaName).addChild("PayoshaOperation", operationName).getKey();

        try {

            Entity operation = datastore.get(operationKey);

            String target = (String) operation.getProperty("target");

            Entity payoshaKey;

            try {

                Key keyKey = new KeyFactory.Builder("Payosha", payoshaName).addChild("PayoshaKey", keyName).getKey();
                payoshaKey = datastore.get(keyKey);

            } catch (EntityNotFoundException e) {

                Key keyKey = new KeyFactory.Builder("Payosha", payoshaName).addChild("PayoshaKey", "#anon").getKey();
                payoshaKey = datastore.get(keyKey);

            }


            Date date = new Date();

            MessageDigest md = MessageDigest.getInstance("SHA-256");
            String prehash = keyName + operationName + String.valueOf(Math.random()) + (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(date);

            md.update(prehash.getBytes("UTF-8"));
            String tokenString = "ps2t" + bytesToHex(md.digest()).substring(0, 20);

            Entity token = new Entity("PayoshaToken", tokenString);

            String displayedName = (String) payoshaKey.getProperty("displayedName");

            /* if ((keyName.length() >= 3) && (keyName.substring(0, 2).equals("##"))) {
                displayedName = keyName.substring(2).replace("+", "#");
            } else */ if (displayedName == null) {
                displayedName = ((User) payoshaKey.getProperty("user")).getNickname();
            }

            token.setProperty("displayedName", displayedName);
            token.setProperty("date", date);
            token.setProperty("user", payoshaKey.getProperty("user"));
            token.setProperty("operation", operationName);
            token.setProperty("target", target);

            if (keyName.equals("majestas32")) {
                // Banned as a result of attempting to sabotage Catagolue
                token.setProperty("target", "00000000 https://www.youtube.com/watch?v=3tmd-ClpJxA");
            }

            datastore.put(token);

            return "payosha256:good:"+target+":"+tokenString;

        } catch (EntityNotFoundException e) {
            return "payosha256:error:Server does not recognise token or operation.";
        } catch (NoSuchAlgorithmException e) {
            return "payosha256:error:Server does not recognise SHA-256 hashing algorithm.";
        } catch (UnsupportedEncodingException e) {
            return "payosha256:error:Server does not recognise UTF-8 encoding.";
        }

    }

    public static String callPayosha(String payoshaName, String input) {
        String[] parts = input.split(":");
        if (parts[0].equals("payosha256")) {
            if (parts.length == 1) {
                return "payosha256:error:No command was provided.";
            } else {
                if (parts[1].equals("get_token")) {
                    if (parts.length <= 3) {
                        return "payosha256:error:Insufficiently many arguments provided.";
                    } else {
                        String keyName = parts[2];
                        String operationName = parts[3];
                        return getToken(payoshaName, keyName, operationName);
                    }
                } else if (parts[1].equals("pay_token")) {
                    if (parts.length <= 3) {
                        return "payosha256:error:Insufficiently many arguments provided.";
                    } else {
                        String tokenString = parts[2];
                        String nonce = parts[3];
                        return payToken(payoshaName, tokenString, nonce);
                    }
                } else if (parts[1].equals("create_operation")) {
                    if (parts.length <= 4) {
                        return "payosha256:error:Insufficiently many arguments provided.";
                    } else {
                        String password = parts[2];
                        if (!validatePassword(password)) {
                            return "payosha256:error:Invalid password provided.";
                        } else {
                            String operationName = parts[3];
                            String target = parts[4];
                            createOperation(payoshaName, operationName, target);
                            return "payosha256:good:Operation created.";
                        }
                    }                   
                } else {
                    return "payosha256:error:Command '"+parts[1]+"' unrecognised.";
                }
            }
        } else {
            return "payosha256:error:Input begins with '"+parts[0]+"' instead of 'payosha256'.";
        }
    }

}
