/* TEAM */
	Creator:    Adam P. Goucher
	Site:       http://cp4space.wordpress.com/
	Twitter:    @apgox
	Instagram:  @apgoucher
	MO:         http://mathoverflow.net/users/39521/adam-p-goucher
	Location:   Cambridge, United Kingdom

/* SITE */

	Site name:  Catagolue
	Site URL:   http://catagolue.appspot.com/
	Built with: Google App Engine